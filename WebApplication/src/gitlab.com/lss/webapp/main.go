package main

import (
	"net/http"
	"text/template"
)

func main() {
	http.ListenAndServe(":8000", http.FileServer(http.Dir("public")))
}

func populateTemplates() *template.Template {
	result := template.New("templates")
	const basePath = "templates"
	template.Must(result.ParseGlob(basePath + "/*.html"))
	return result
}
